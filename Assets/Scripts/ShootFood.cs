using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootFood : MonoBehaviour
{
    [SerializeField] private float foodSpeed;

    void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * foodSpeed);
    }
}
