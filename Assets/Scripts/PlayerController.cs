using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float movementSpeed;
    [SerializeField] private GameObject food;
    private float horizontalInput;
    private float xRange = 12;

    void Update()
    {
        horizontalInput = Input.GetAxis("Horizontal");
        transform.Translate(Vector3.right * Time.deltaTime * horizontalInput * movementSpeed);
        if (transform.position.x < -xRange)
        {
            transform.position = new Vector3(-xRange, 0, 0);
        }
        else if (transform.position.x > xRange)
        {
            transform.position = new Vector3(xRange, 0, 0);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Instantiate(food, transform.position, food.transform.rotation);
        }
    }
}
