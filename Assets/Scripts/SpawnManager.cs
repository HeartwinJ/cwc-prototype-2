using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    [SerializeField] private GameObject[] animals;
    [SerializeField] private float spawnRangeX;

    void Start()
    {
        InvokeRepeating("SpawnRandomAnimal", 2f, 1.5f);
    }

    void SpawnRandomAnimal()
    {
        int animalIndex = Random.Range(0, animals.Length);
        float spawnPos = Random.Range(-spawnRangeX, spawnRangeX);
        Instantiate(animals[animalIndex], new Vector3(spawnPos, 0, 25), animals[animalIndex].transform.rotation);
    }
}
