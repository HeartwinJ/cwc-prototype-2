using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestoryOutOfBounds : MonoBehaviour
{
    [SerializeField] private float topBound;
    [SerializeField] private float bottomBound;

    void Update()
    {
        if (transform.position.z > topBound || transform.position.z < bottomBound)
        {
            Destroy(gameObject);
            Debug.Log("Game Over!");
        }
    }
}
